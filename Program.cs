﻿using System;

namespace AlgoDat
{
    class Program
    {

        static void Main(string[] args)
        {
            AoCSolver test = new AoCSolver(args[0]);
            test.GetPuzzleGraph();
            test.ComputeShortestPath();
            test.ShortestPathValue();
            
            foreach (var item in test.ShortestPath())
            {
                System.Console.Write(item + " , ");
            }
        }
    }
}
