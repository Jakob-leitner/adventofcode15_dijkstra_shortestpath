using System;
using System.IO;
using System.Collections.Generic;

namespace AlgoDat
{/// <summary>Solves part 1, day 15 puzzle from Advent of Code 2021</summary>
    public class AoCSolver
    {
        int[,] board;
        public WeightedGraph<string> graph = new WeightedGraph<string>();
        List<string> list = new List<string>();

        /// <summary>Loads the AoC 2021 day 15 puzzle input
        /// from a file</summary>
        /// <param name="inputFile">AoC input file</param>
        public AoCSolver(string inputFile)
        {
            var temp = File.ReadAllLines(inputFile);

            board = new int[temp[0].Length, temp.Length];

            for (int i = 0; i < temp[0].Length; i++)
            {
                for (int j = 0; j < temp.Length; j++)
                {
                    board[i, j] = int.Parse(temp[i][j].ToString());
                }
            }
            // System.Console.WriteLine(string.Join("\n",board));
        }
        /// <summary>
        /// Get the graph representation of the puzzle input.
        /// The nodes are strings with the format "<colNum>;<rowNum>"
        /// where the numbering starts at 0.
        /// </summary>
        /// <returns>The weighted graph representation of the input</returns>
        public WeightedGraph<string> GetPuzzleGraph()
        {

            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 1; j < board.GetLength(1); j++)
                {
                    graph.AddEdge($"{j - 1};{i}", $"{j};{i}", (double)board[j, i]);
                    graph.AddEdge($"{i};{j - 1}", $"{i};{j}", (double)board[i, j]);
                }
            }
            return graph;
        }
        /// <summary>Compute the shortest path from 0;0 to <maxCol>;<maxRow>
        /// according to the game rules</summary>
        public void ComputeShortestPath()
        {
            list = ShortestPath<string>.Compute(graph, "0;0", $"{board.GetLength(0)-1};{board.GetLength(1)-1}");
        }
        /// <summary>Return the shortest path value</summary>
        /// <returns>The shortest path / sum of all edges in the
        /// puzzle result</returns>
        public int ShortestPathValue()
        {
            int sum = 0;

            for (int i = 0; i < list.Count; i++)
            {
                var buffer = list[i].Split(';');
                var firstIndex = buffer[0];
                var secondIndex = buffer[1];
                sum += board[(int.Parse(firstIndex)), int.Parse(secondIndex)];
            }
            sum-=board[0,0];
            System.Console.WriteLine(sum);
            int summe = 0;
            for (int i = 1; i < list.Count; i++)
            {
           summe += (int)graph.GetEdges(list[i-1]).Search(new WeightedGraph<string>.Edge(list[i-1],list[i],5)).Key.Weight;
                
            }
            System.Console.WriteLine("SUMME: " + summe);

            
            return sum;
        }
        /// <summary>Return the steps / cells of the shortest path beginning
        /// from the start field</summary>
        public DoubleLinkedList<string> ShortestPath()
        {
            var output = new DoubleLinkedList<string>();

            foreach (var item in list)
            {
                output.Append(item);
            }
            return output;
        }
    }
}